#__autoras__ : "Zein Gualsaqui, Ingrid Guaraca, Cinthia Valdez y Gabriela Viñan"
#__email__ : "zein.gualsaqui@unl.edu.ec, ingrid.guaraca@unl.edu.ec, cinthia.valdez@unl.edu.ec, gabriela.vinan@unl.edu.ec"
# División y Multiplicación de números decimales, Sucesiones.

class division_decimales:
    numero1= 0.0
    numero2= 0.0
    def __init__(self, numero1, numero2):
        self.numero1 = numero1
        self.numero2= numero2
    def resultado (self) :
        self.resul = self.numero1 / self.numero2
        return self.resul

class multiplicacion_decimales:
    n1= 0.0
    n2= 0.0
    def __init__(self, n1, n2):
        self.n1 = n1
        self.n2= n2
    def resultado (self) :
        self.resul = self.n1 * self.n2
        return self.resul

class Sucesiones_suma:
    a= int
    b= int
    def __init__(self,a, b, n):
        self.a= a
        self.b= b
        self.n= n
        for i in range(0, n):
            b += a
            a = b-a
            print(a, end = " , ")
        if a > 0:
            print("Creciente")
        else:
            print("Decreciente")

class Sucesiones_resta:
    a = int
    b = int
    def __init__(self, a, b, n):
        self.a = a
        self.b = b
        self.n = n
        for i in range(0, n):
            b -= a

            print(b, end=" , ")
        if b > 0:
            print("Creciente")
        else:
            print("Decreciente")

class Sucesiones_multiplicacion:
    primertermino= int
    razon= int
    def __init__(self, primertermino, razon, n):
        self.primertermino = primertermino
        self.razon = razon
        self.n = n
        for i in range(0, n):
            razon *= primertermino
            print(razon, end = " , ")
        if razon > 0:
            print("Creciente")
        else:
            print("Decreciente")
class Sucesiones_division:
    primertermino= int
    razon= int
    def __init__(self, primertermino, razon, n):
        self.primertermino = primertermino
        self.razon = razon
        self.n = n
        for i in range(0, n):
            razon /= primertermino
            print(razon, end = " , ")
        if razon > 0:
            print("Creciente")
        else:
            print("Decreciente")
